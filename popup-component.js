(function($) {
    $(function() {
        var $body = $('body');
        var $component = $('.help-and-training-area');
        $body.append($component.show());
        $('.help-and-training-area__handle').click(function() {
            $(this).closest('.help-and-training-area').toggleClass('opened');
        });

        // chat
        var $chat = $('.chat'),
            $chatTextarea = $('.chat__textarea textarea'),
            $chatClose = $('.chat__close'),
            $chatHandle = $('.chat-handle'),
            $chatList = $('.chat__list'),
            msg =   '<div class="msg">'+
                        '<div class="msg__time"></div>'+
                        '<div class="msg__text"></div>'+
                    '</div>',
            msgRevert =     '<div class="msg msg_revert">'+
                                '<div class="msg__text"></div>'+
                                '<div class="msg__time"></div>'+
                            '</div>',
            answered = false;
            
        $body.append($chat);
        $chatTextarea.keydown(function(event) {
            if (event.keyCode == 13) {
                $(this).closest('form').submit()
                return false;
             }
        }).closest('form').submit(function() {
            if (!$chatTextarea.val()) return false;
            var $msg = $(msg);
            
            $msg.find('.msg__time').html(getTime(new Date()));
            $msg.find('.msg__text').html($chatTextarea.val());
            
            $chatList.append($msg);
            
            $chatTextarea.val('');
            
            if (!answered) {
                setTimeout(function() {
                    answered = true;
                    var $msg = $(msgRevert);
                
                    $msg.find('.msg__time').html(getTime(new Date()));
                    $msg.find('.msg__text').html('Hello dear User! This is the answer\'s example; Some friendly text are here');
                    $chatList.append($msg);
                }, 1000);
            }
            return false;
        });
        
        $chatClose.click(function() {
            $chat.hide();
        });
        $chatHandle.click(function() {
            $chat.show();
        });

        // end of popup
        function getTime(date) {
            var hours = date.getHours()
                minutes = date.getMinutes();
            return (hours < 10 ? '0' + hours : hours) + ':' + (minutes < 10 ? '0' + minutes : minutes)
        }
    });
})(jQuery);
